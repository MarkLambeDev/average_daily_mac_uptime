import subprocess
import datetime

total_uptime_hours = subprocess.check_output("smartctl -a disk0 | grep Power_On_Hours | awk '{print $10}'", shell=True)

bought_date = datetime.datetime(2015, 6, 19)
now = datetime.datetime.now()
delta = now - bought_date

hours_per_day = round(float(total_uptime_hours) / float(delta.days),2)

hours = int(hours_per_day)
minutes = int((hours_per_day-hours) * 60)

print "Since you bought this machine, it's been on for an average of %s hours and %s minutes per day." % (hours, minutes)



